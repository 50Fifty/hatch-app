# APIs

## Tasks

- Path: `/api/tasks`, `/api/tasks/<int:pk>`
- Method: `GET`, `POST`, `PUT`, `DELETE`

Input:

```
{
"name": string,
"start_time": string,
"finished": boolean,
"category_id": int,
"account_id": int,
"pom_finished": int
}

```

Output:

```
{
"id": int,
"name": string,
"start_time": string,
"finished": boolean,
"category_id": int,
"account_id": int,
"pom_finished": int
}
```

## Categories

- Path: `/api/categories`
- Method: `GET`

Output:

```
{
"id": int,
"name": string
}

```

## Difficulties

- Path: `/api/difficulties`
- Method: `GET`

Output:

```
  {
    "id": int,
    "level": string,
    "work_time": int,
    "break_time": int
  }

```

## Accounts

- Path: `/api/accounts`
- Method: `PUT`, `POST`

Input (POST)

```
{
  "username": string,
  "password": string,
  "email": string,
  "first": string,
  "last": string,
  "avatar": string,
  "reason": string,
  "difficulty_id": int
}
```

Input (PUT)

```
{
  "username": string,
  "password": string,
  "email": string,
  "first": string,
  "last": string,
  "avatar": string,
  "reason": string,
  "difficulty_id": int,
  "id": int,
  "hashed_password": string
}
```

- Path: `/api/accounts/<username>`
- Method: `GET`

Input:

```
{
  "access_token": string,
  "token_type": "Bearer",
  "account": {
    "id": int,
    "username": string,
    "email": string,
    "first": string,
    "last": string,
    "avatar": string,
    "reason": string,
    "difficulty_id": int
  }
}
```
