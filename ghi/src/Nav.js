import Container from "react-bootstrap/Container";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import { Link } from "react-router-dom"; // <-- Added an import for react Link

// --------------------- SMALL BUT IMPORTANT CHANGE IN NAV ---------------------------
// Because you're using a custom bootstrap navbar instead of the built-in
// Link component that comes with react, your links are actually causing
// your site to reload entirely, clearing your state every time you click
// a link. This works counter-intuitively to how react is supposed to work
// and can cause headaches when you want to do things like check if a user
// is logged in before accessing a page, because the state that is supposed
// to store that user information is cleared each time you hit the page.

// To fix this, you simply need to import Link, add "as={Link}" to your Nav.Link
// components, and change "href=" to "to=". This will change your Nav.Link
// components to act as if they are standard Link components from the react library.
// -----------------------------------------------------------------------------------

function NavBar(props) {
  return (
    <Navbar bg="dark" expand="lg" variant="dark" sticky="top">
      <Container fluid>
        <Navbar.Brand>
          <img
            alt=""
            src={"../HATCH.png"}
            width="85"
            height="auto"
            className="d-inline-block align-top"
          />
        </Navbar.Brand>
        <Navbar.Brand
          as={Link}
          to={props.account && props.account.id ? "main" : "/"}
        >
          Hatch App
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="navbarScroll" />
        <Navbar.Collapse id="navbarScroll">
          <Nav
            className="me-auto my-2 my-lg-0"
            style={{ maxHeight: "100px" }}
            navbarScroll
          >
            {props.account && props.account.id ? (
              <>
                <Nav.Link as={Link} to="profile">
                  Profile
                </Nav.Link>
                <Nav.Link as={Link} to="logout">
                  Logout
                </Nav.Link>
              </>
            ) : (
              <Nav.Link as={Link} to="login">
                Login
              </Nav.Link>
            )}
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}

export default NavBar;
