import React from "react";

class AccountForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      firstName: "",
      lastName: "",
      email: "",
      userName: "",
      reason: "",
      difficulty: "",
    };
    this.handleFirstNameChange = this.handleFirstNameChange.bind(this);
    this.handleLastNameChange = this.handleLastNameChange.bind(this);
    this.handleEmailChange = this.handleEmailChange.bind(this);
    this.handleUserNameChange = this.handleUserNameChange.bind(this);
    this.handleReasonChange = this.handleReasonChange.bind(this);
    this.handleDifficultyChange = this.handleDifficultyChange.bind(this);
    // this.handleSubmit = this.handleSubmit.bind(this)
  }
  handleFirstNameChange(event) {
    const value = event.target.value;
    this.setState({ firstname: value });
  }
  handleLastNameChange(event) {
    const value = event.target.value;
    this.setState({ lastname: value });
  }
  handleEmailChange(event) {
    const value = event.target.value;
    this.setState({ email: value });
  }
  handleUserNameChange(event) {
    const value = event.target.value;
    this.setState({ username: value });
  }
  handleReasonChange(event) {
    const value = event.target.value;
    this.setState({ reason: value });
  }
  handleDifficultyChange(event) {
    const value = event.target.value;
    this.setState({ difficulty_id: value });
  }
  // async handleSubmit(event){
  //     event.preventDefault();
  //     const data = {...this.state};

  //     const accountUrl = 'http://localhost:8000/api/hatch--app/';
  //     const fetchConfig = {
  //         method: "post",
  //         body: JSON.stringify(data),
  //         headers: {
  //             'Content-Type': 'application/json'
  //         },
  //     };
  //     const response = await fetch(accountUrl, fetchConfig);
  //     if (response.ok) {
  //         const cleared = {
  //             firstName: "",
  //             lastName: "",
  //             email: "",
  //             userName: "",
  //             reason: "",
  //             difficulty: "",
  //         };
  //         this.setState(cleared);
  //     };
  // };

  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create Account</h1>
            <form onSubmit={this.handleSubmit} id="new-account-form">
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleFirstNameChange}
                  value={this.state.firstName}
                  placeholder="first"
                  required
                  type="text"
                  name="name"
                  id="first"
                  className="form-control"
                />
                <label htmlFor="firstName">First Name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleLastNameChange}
                  value={this.state.lastName}
                  placeholder="last"
                  required
                  type="text"
                  name="name"
                  id="last"
                  className="form-control"
                />
                <label htmlFor="lastName">Last Name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleEmailChange}
                  value={this.state.email}
                  placeholder="Email"
                  required
                  type="text"
                  name="email"
                  id="email"
                  className="form-control"
                />
                <label htmlFor="email">Email</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleUserNameChange}
                  value={this.state.userName}
                  placeholder="Username"
                  required
                  type="text"
                  name="username"
                  id="username"
                  className="form-control"
                />
                <label htmlFor="Username">Username</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleReasonChange}
                  value={this.state.reason}
                  placeholder="Reason"
                  required
                  type="text"
                  name="reason"
                  id="reason"
                  className="form-control"
                />
                <label htmlFor="reason">Reason</label>
              </div>
              <div className="form-floating mb-3">
                <select
                  onChange={this.handleDifficultyChange}
                  value={this.state.difficulty}
                  required
                  id="difficulty"
                  name="difficulty"
                  className="form-select"
                >
                  <option value="">Choose a sales person</option>
                  {this.state.difficulty.map((difficulty) => {
                    return (
                      <option key={difficulty.id} value={difficulty.id}>
                        {difficulty.name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary" id="signupBtn">
                Create Account
              </button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}
export default AccountForm;
