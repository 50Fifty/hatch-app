import React, { useState, useEffect } from "react";
import Form from "react-bootstrap/Form";

const TaskContainer = (props) => {
  const { setTaskData, accountData } = props;
  const [taskName, setTaskName] = useState("");
  const [categories, setCategories] = useState([]);
  const [category_id, setCategoryID] = useState([]);
  const [categoryName, setCategoryName] = useState([]);
  const [numPomo, setNumPomo] = useState("");
  const [error, setError] = useState(null);

  // Updates taskData, when 'select this as Key Task' button clicked
  const sendData = () => {
    const newStartTime = Date();
    setTaskData({
      name: taskName,
      start_time: newStartTime,
      finished: false,
      category_id: category_id,
      account_id: accountData.id,
      categoryName: categoryName,
      pom_finished: numPomo,
    });
  };

  // get categories for dropdown
  useEffect(() => {
    async function getCategories() {
      let url = `${process.env.REACT_APP_API_HOST}/api/categories`;
      let response = await fetch(url);
      let data = await response.json();
      if (response.ok) {
        setCategories(data);
      } else {
        setError("Could not load categories");
        console.log(error);
      }
    }
    getCategories();
    //eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  // handles category dropdown change, setting state for cat ID and name
  const handleCatChange = (event) => {
    setCategoryID(event.target.value);
    for (let category of categories) {
      if (category.id === parseInt(event.target.value)) {
        setCategoryName(category.name);
      }
    }
  };
  return (
    <>
      <div className="taskContainer">
        <form>
          <div className="row">
            <input
              type="task"
              placeholder="Task Name"
              onChange={(event) => setTaskName(event.target.value)}
            />
            <input
              type="number"
              placeholder="#"
              onChange={(event) => setNumPomo(event.target.value)}
            />
            <label></label>
          </div>
          <div className="dropdown-category">
            <Form.Select
              size="sm"
              onChange={handleCatChange}
              required
              name="categories"
              id="categories"
            >
              <option value="">Select Category</option>
              {categories.map((categories) => {
                return (
                  <option key={categories.id} value={categories.id}>
                    {categories.name}
                  </option>
                );
              })}
            </Form.Select>
          </div>
          <div className="keybutton">
            <button
              type="button"
              className="btn btn-outline-success"
              onClick={sendData}
            >
              Select this as Key Task
            </button>
          </div>
        </form>
      </div>
    </>
  );
};

export default TaskContainer;
