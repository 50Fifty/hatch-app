import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { Modal, Button } from "react-bootstrap";
import BarGraph from "./BarGraph";
import PieChart from "./PieChart";
import "./profile.css";

function BootstrapInputFields(props) {
  const { id, label, value, onChange, type, placeholder } = props;

  return (
    <div className="mb-3">
      <label htmlFor={id} className="form-label">
        {label}
      </label>
      <input
        value={value}
        onChange={onChange}
        required
        type={type}
        className="form-control"
        id={id}
        placeholder={placeholder}
      />
    </div>
  );
}

function AccountUpdateModal(props) {
  const [account, setAccount] = useState({ ...props.account, password: "" });
  const [errorMessage, setErrorMessage] = useState(false);

  async function handleSubmit(e) {
    e.preventDefault();

    try {
      props.update(account);
      setErrorMessage(false);
      props.setModalShow(false);
    } catch (err) {
      setErrorMessage(true);
    }
  }

  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Update Account
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <form onSubmit={handleSubmit}>
          <BootstrapInputFields
            id="first"
            label="Enter First Name"
            value={account.first}
            onChange={(e) =>
              setAccount({
                ...account,
                first: e.target.value,
              })
            }
            type="text"
            placeholder="First name"
          />
          <BootstrapInputFields
            id="last"
            label="Enter Last Name"
            value={account.last}
            onChange={(e) =>
              setAccount({
                ...account,
                last: e.target.value,
              })
            }
            type="text"
            placeholder="Last name"
          />
          <BootstrapInputFields
            id="userName"
            label="Enter Username"
            value={account.username}
            onChange={(e) =>
              setAccount({
                ...account,
                username: e.target.value,
              })
            }
            type="text"
            placeholder="Username"
          />
          <BootstrapInputFields
            id="password"
            label="Enter Password"
            value={account.password}
            onChange={(e) =>
              setAccount({
                ...account,
                password: e.target.value,
              })
            }
            type="text"
            placeholder="Password"
          />
          <BootstrapInputFields
            id="email"
            label="Enter Email Address"
            value={account.email}
            onChange={(e) =>
              setAccount({
                ...account,
                email: e.target.value,
              })
            }
            type="email"
            placeholder="name@website.com"
          />
          <BootstrapInputFields
            id="avatar"
            label="Choose a profile picture"
            value={account.avatar}
            onChange={(e) =>
              setAccount({
                ...account,
                avatar: e.target.value,
              })
            }
            type="text"
            placeholder="Picture"
          />
          <BootstrapInputFields
            id="reason"
            label="Reason for using timer"
            value={account.reason}
            onChange={(e) =>
              setAccount({
                ...account,
                reason: e.target.value,
              })
            }
            type="text"
            placeholder="Reason"
          />
          <BootstrapInputFields
            id="difficulty_id"
            label="Difficulty (select 1, 2, 3)"
            value={account.difficulty_id}
            onChange={(e) =>
              setAccount({
                ...account,
                difficulty_id: e.target.value,
              })
            }
            type="text"
            placeholder="1:[15min focus|15min break], 2:[20|10], 3:[25|5]"
          />
          <button type="submit" className="btn btn-outline-success">
            Submit
          </button>
          <div className="text-center mt-4" style={{ color: "red" }}>
            {errorMessage ? <h5>Email or Username already exists</h5> : ""}
          </div>
        </form>
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={props.onHide}>Close</Button>
      </Modal.Footer>
    </Modal>
  );
}

function ProfilePage(props) {
  const [pageShow, setPageShow] = useState(false); // <-- Added variable that determines if page should render.
  const navigate = useNavigate();
  const [modalShow, setModalShow] = useState(false);

  // ---------------------------------- REDIRECT TO LOGIN -------------------------------------
  // On page load, checks to see if an account was passed through props.
  // If no account was passed, page is redirected to login.
  // If an account was passed, pageShow is set to true, which allows the page to render.

  useEffect(() => {
    if (typeof props.account !== "object") {
      console.log("user not logged in, redirect to login page");
      console.log(props.account);
      navigate("/login");
    } else if (props.account.id) {
      setPageShow(true);
    }
  }, [navigate, props.account]);

  // While it may seem like you can write this without using useEffect or a pageShow variable,
  // it's not good practice to use navigate() immediately on page render, as it can cause
  // some problems and won't work consistently.
  // -----------------------------------------------------------------------------------------

  if (pageShow) {
    // <-- Added conditional so that page does not render unless pageShow is true.
    return (
      <>
        <div className="profileContainer">
          {/* <div style={{ color: "white" }}> */}
          <div className="Greeting">
            <h1>
              Hey {props.account.first} {props.account.last}!
            </h1>
          </div>
          <div className="Reason">
            <h2
              style={{
                color: "#FDFDBD",
                justifyContent: "center",
                fontStyle: "italic",
                fontSize: "20px"
              }}
            >
              Remember why you're here...
            </h2>
            <h2
              style={{
                color: "white",
              }}
            >
              {props.account.reason}
            </h2>
          </div>
          <div className="Avatar">
            <img
              className="imageAvatar"
              type="avatarImage"
              src={props.account.avatar}
              alt="avatar"
            ></img>
            <div className="updateButton">
              <Button
                variant="btn btn-outline-success"
                onClick={() => setModalShow(true)}
              >
                Update Account
              </Button>
            </div>
          </div>
          <div className="Stat_1">
            <h3 className="mt-4 text-center">
              Number of Tasks based on the day of the week
            </h3>
            <BarGraph accountId={props.account.id} />
          </div>
          <div className="Stat_2">
            <h3 className="mt-4 text-center">
              Tasks Accomplished based on category
            </h3>
            <PieChart accountId={props.account.id} />
          </div>

          {/* </div> */}
          <AccountUpdateModal
            show={modalShow}
            onHide={() => setModalShow(false)}
            account={props.account}
            setModalShow={setModalShow}
            update={props.update}
          />
        </div>
      </>
    );
  }
}

export default ProfilePage;
