const KeyTaskContainer = (props) => {
  const { taskData } = props;

  return (
    <>
      <div style={{ color: "white" }}>
        <form>
          <div>
            Task: {taskData.name}
            <label></label>
          </div>
          <div>
            Pomodoros Remaining:{" "}
            {taskData.pom_finished === 0
              ? "Update Key Task"
              : taskData.pom_finished}
          </div>
        </form>
      </div>
    </>
  );
};

export default KeyTaskContainer;
