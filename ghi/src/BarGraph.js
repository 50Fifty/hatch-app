import { useEffect, useState } from "react";
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend,
} from "chart.js";

import { Bar } from "react-chartjs-2";
ChartJS.register(
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend
);

const BarGraph = (props) => {
  const { accountId } = props;

  const [graphData, setGraphData] = useState({
    labels: ["Sun", "Mon", "Tue", "Wed", "Thur", "Fri", "Sat"],
    datasets: [
      {
        label: "Tasks per Days of the Week",
        data: [],
        backgroundColor: [
          "rgb(124, 120, 106)",
          "rgb(141, 205, 193)",
          "rgb(211, 227, 151)",
          "rgb(255, 245, 195)",
          "rgb(235, 110, 68)",
        ],
        hoverOffset: 4,
      },
    ],
  });

  function produceTaskCountPerDay(allTasks) {
    let Sun = 0;
    let Mon = 0;
    let Tue = 0;
    let Wed = 0;
    let Thur = 0;
    let Fri = 0;
    let Sat = 0;
    for (let task of allTasks) {
      // counters for specific categories
      if (accountId === task["account_id"] && task["finished"] === true) {
        let dateString = task["start_time"].substring(0, 3);
        if (dateString === "Sun") {
          Sun++;
        } else if (dateString === "Mon") {
          Mon++;
        } else if (dateString === "Tue") {
          Tue++;
        } else if (dateString === "Wed") {
          Wed++;
        } else if (dateString === "Thu") {
          Thur++;
        } else if (dateString === "Fri") {
          Fri++;
        } else if (dateString === "Sat") {
          Sat++;
        }
      }
    }
    const dataSet = [Sun, Mon, Tue, Wed, Thur, Fri, Sat];
    return dataSet;
  }

  useEffect(() => {
    async function getTasks() {
      const urlTasks = `${process.env.REACT_APP_API_HOST}/api/tasks`;
      let responseTasks = await fetch(urlTasks);
      if (responseTasks.ok) {
        const allTasks = await responseTasks.json();
        let dataSet = produceTaskCountPerDay(allTasks);
        setGraphData({
          labels: ["Sun", "Mon", "Tue", "Wed", "Thur", "Fri", "Sat"],
          datasets: [
            {
              label: "Tasks per day of the week",
              data: dataSet,
              backgroundColor: [
                "rgb(124, 120, 106)",
                "rgb(141, 205, 193)",
                "rgb(211, 227, 151)",
                "rgb(255, 245, 195)",
                "rgb(235, 110, 68)",
              ],
              hoverOffset: 4,
            },
          ],
        });
      } else {
        console.log("no tasks retrieved");
      }
    }
    getTasks();
    //eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div className="chartreturn">
      <Bar
        data={graphData}
        options={{
          color: "white",
          scales: {
            x: {
              ticks: {
                color: "white",
              },
              grid: {
                color: "#B4A5A5",
                borderColor: "white",
              },
            },
            y: {
              ticks: {
                color: "white",
              },
              grid: {
                color: "#B4A5A5",
                borderColor: "white",
              },
            },
          },
          plugins: {
            legend: {
              labels: {
                color: "white",
              },
            },
          },
        }}
      />
    </div>
  );
};

export default BarGraph;
