import React, { useState, useEffect } from "react";

// import './App.css';
import { Chart as ChartJs, Tooltip, Title, ArcElement, Legend } from "chart.js";
import { Doughnut } from "react-chartjs-2";
ChartJs.register(Tooltip, Title, ArcElement, Legend);

const PieChart = (props) => {
  const { accountId } = props;

  const [graphData, setGraphData] = useState({
    labels: ["1", "2", "3", "4", "5"],
    datasets: [
      {
        label: "Tasks per categories",
        data: [],
        backgroundColor: [
          "rgb(124, 120, 106)",
          "rgb(141, 205, 193)",
          "rgb(211, 227, 151)",
          "rgb(255, 245, 195)",
          "rgb(235, 110, 68)",
        ],
        hoverOffset: 4,
      },
    ],
  });

  function produceTaskCount(allTasks) {
    let a = 0;
    let b = 0;
    let c = 0;
    let d = 0;
    let e = 0;
    for (let task of allTasks) {
      // counters for specific categories
      if (accountId === task["account_id"] && task["finished"] === true) {
        if (task["category_id"] === 1) {
          a++;
        } else if (task["category_id"] === 2) {
          b++;
        } else if (task["category_id"] === 3) {
          c++;
        } else if (task["category_id"] === 4) {
          d++;
        } else if (task["category_id"] === 5) {
          e++;
        }
      }
    }
    const dataSet = [a, b, c, d, e];
    return dataSet;
  }

  useEffect(() => {
    async function getTasks() {
      const urlTasks = `${process.env.REACT_APP_API_HOST}/api/tasks`;
      const urlCategories = `${process.env.REACT_APP_API_HOST}/api/categories`;
      let responseTasks = await fetch(urlTasks);
      let responseCategories = await fetch(urlCategories);
      if (responseTasks.ok && responseCategories.ok) {
        const allTasks = await responseTasks.json();
        const allCategories = await responseCategories.json();
        let dataSet = produceTaskCount(allTasks);

        // map out list of categories, to be used as label
        const labelCategories = allCategories.map(
          (category) => category["name"]
        );

        // setup for only 5 categories
        setGraphData({
          labels: labelCategories,
          datasets: [
            {
              label: "Tasks per categories",
              data: dataSet,
              backgroundColor: [
                "rgb(124, 120, 106)",
                "rgb(141, 205, 193)",
                "rgb(211, 227, 151)",
                "rgb(255, 245, 195)",
                "rgb(235, 110, 68)",
              ],
              hoverOffset: 4,
            },
          ],
        });
      } else {
        console.log("no tasks retrieved");
      }
    }
    getTasks();
    //eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div>
      <Doughnut
        data={graphData}
        options={{
          color: "white",
          plugins: {
            legend: {
              labels: {
                color: "white",
              },
            },
          },
        }}
      />
    </div>
  );
};

export default PieChart;
