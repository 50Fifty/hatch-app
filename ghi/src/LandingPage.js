import React from "react";

import TimerLandingPage from "./TimerLandingPage";

function LandingPage() {
  return <TimerLandingPage />;
}
export default LandingPage;
