from urllib import response
from fastapi import (
    Depends,
    HTTPException,
    status,
    Response,
    APIRouter,
    Request,
)
from jwtdown_fastapi.authentication import Token
from authenticator import authenticator, AccountAuth

from pydantic import BaseModel

from queries.accounts import (
    AccountIn,
    AccountInWithIdAndHash,
    AccountOut,
    AccountQueries,
    DuplicateAccountError,
)


class AccountForm(BaseModel):
    username: str
    email: str
    password: str
    first: str
    last: str
    avatar: str
    reason: str
    difficulty_id: int


class AccountToken(Token):
    account: AccountOut


class HttpError(BaseModel):
    detail: str


router = APIRouter()


@router.get("/api/protected", response_model=bool)
async def get_protected(
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    return True


@router.get("/token", response_model=AccountToken | None)
async def get_token(
    request: Request,
    account: AccountOut = Depends(authenticator.try_get_current_account_data),
) -> AccountToken | None:
    if authenticator.cookie_name in request.cookies:
        return {
            "access_token": request.cookies[authenticator.cookie_name],
            "type": "Bearer",
            "account": account,
        }


@router.patch("/token", response_model=AccountToken | HttpError)
async def update_account(
    info: AccountIn,
    request: Request,
    response: Response,
    accounts: AccountQueries = Depends(),
):
    hashed_password = authenticator.hash_password(info.password)
    account = accounts.update(info, hashed_password)
    form = AccountForm(
        username=info.username,
        email=info.email,
        first=info.first,
        last=info.last,
        avatar=info.avatar,
        reason=info.reason,
        difficulty_id=info.difficulty_id,
    )
    token = await authenticator.update(response, request, form, accounts)
    return AccountToken(account=account, **token.dict())


async def get_auth():
    return authenticator


@router.post("/api/accounts", response_model=AccountToken | HttpError)
async def create_account(
    info: AccountIn,
    request: Request,
    response: Response,
    accounts: AccountQueries = Depends(),
    authenticator: AccountAuth = Depends(get_auth),
):
    hashed_password = authenticator.hash_password(info.password)
    try:
        account = accounts.create(info, hashed_password)
    except DuplicateAccountError:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Cannot create an account with those credentials",
        )
    form = AccountForm(
        username=info.username,
        email=info.email,
        password=info.password,
        first=info.first,
        last=info.last,
        avatar=info.avatar,
        reason=info.reason,
        difficulty_id=info.difficulty_id,
    )
    token = await authenticator.login(response, request, form, accounts)
    return AccountToken(account=account, **token.dict())


# concept used before using token
@router.get("/api/accounts/{username}", response_model=AccountToken)
async def get_account(
    username: str,
    response: Response,
    queries: AccountQueries = Depends(),
):
    record = queries.get(username)
    if record is None:
        response.status_code = 404
    else:
        return response


@router.put("/api/accounts", response_model=AccountToken | HttpError)
async def update_account(
    info: AccountInWithIdAndHash,
    request: Request,
    response: Response,
    accounts: AccountQueries = Depends(),
):
    if info.password:
        hashed_password = authenticator.hash_password(info.password)
    else:
        hashed_password = info.hashed_password
    try:
        account = accounts.update(info, hashed_password)
    except DuplicateAccountError:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Cannot create an account with those credentials",
        )
    form = AccountForm(
        username=info.username,
        email=info.email,
        password=info.password,
        first=info.first,
        last=info.last,
        avatar=info.avatar,
        reason=info.reason,
        difficulty_id=info.difficulty_id,
    )
    token = await authenticator.login(response, request, form, accounts)
    return AccountToken(account=account, **token.dict())
