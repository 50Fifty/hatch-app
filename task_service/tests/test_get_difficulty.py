# import fastAPI's built in testing client
from fastapi.testclient import TestClient
from main import app
from queries.tasks import DifficultyRepository, DifficultyOut

# basic structure of difficulty for reference
# class DifficultyOut(BaseModel):
#   id: int
#   level: str
#   work_time: int
#   break_time: int

# this is what we expect to get returned
# get single difficulty
diff_get = DifficultyOut(id=1, level="blah", work_time="15", break_time="15")
difficulty_get_list = [diff_get]


client = TestClient(app)


class MockDifficultyQueries:
    def get_difficulties(self):
        return difficulty_get_list

# Act
def test_get_difficulty():
    # using MockDiff instead of actual Diff (this is what dependency overrides does)
    app.dependency_overrides[DifficultyRepository] = MockDifficultyQueries
    response = client.get("/api/difficulties")

    assert response.status_code == 200
    assert response.json() == difficulty_get_list

    # clear out dependency overrides
    app.dependency_overrides = {}


# Assert


# to test this:
# enter into cli of task-service
# cd into tests dir
# type python -m pytest
