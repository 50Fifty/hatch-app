from fastapi.testclient import TestClient
from main import app
from queries.tasks import TaskRepository

client = TestClient(app)


class MockTaskQueries:
    def delete_task(self, item):
        return True


req_body_good = {
    "id": 1,
    "name": "str",
    "start_time": "str",
    "finished": True,
    "category_id": 1,
    "account_id": 1,
    "pom_finished": 1,
}
req_body_bad = {
    "id": 1,
    "name": "str",
    "start_time": "str",
    "finished": True,
    "category_id": 1,
}

bad_del = {"detail": [{"loc": ["string", 0], "msg": "string", "type": "string"}]}

task_id = 1
task_pk = "string"


def test_delete_task():
    app.dependency_overrides[TaskRepository] = MockTaskQueries
    response = client.delete(f"/api/tasks/{task_id}", json=req_body_good)

    assert response.status_code == 200
    assert response.json() == True

    try:
        response = client.post(f"/api/tasks/{task_pk}", json=req_body_bad)
    except:
        assert response.status_code == 422
        assert response.json() == bad_del

    app.dependecy_overrides = {}
