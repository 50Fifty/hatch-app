# # need to run Docker commands to actually run these tests.
# # you can't just run pytest doc_name, 
# because these files depend on the Docker container running

# # import the entire app
# from main import app

# # import fastAPI's built in testing client
# from fastapi.testclient import TestClient
# from task_service.queries.tasks import TaskRepository

# # wrap app in test client
# client = TestClient(app)


# ## dependency injection - create mock dependency for Task Repo
# # get tasks depends on the creation of a TaskRpo instance
# # for unit tests - we need to set this all up
# class MockTaskRepository:
#   def get_tasks():
#     return [task]

# task = {
#         'id': 1,
#   'name': 'test',
#   'start_time': '11:08',
#   'num_of_pomodoros': 1,
#   'category_id': 1,
#   'user_id': 1
#     }

# def test_get_tasks_empty():
#   # dependency injection, because
#   app.dependency_overrides(TaskRepository)= MockTaskRepository
#   response = client.get('/api/tasks')

#   assert response.status_code == 200
#   assert response.json() == {'tasks': [tasks] }
#   # AAA, and then cleanup

# # cleanup, we need to cleanup any persistent data
#   app.dependency_overrides(TaskRepository)= {}

from fastapi.testclient import TestClient
from main import app
from queries.tasks import TaskRepository, TaskOut


task_get = TaskOut(
    id=1,
    name="string",
    start_time="string",
    finished=True,
    category_id=1,
    account_id=1,
    pom_finished=1,
)
task_get_list = [task_get]

client = TestClient(app)

task = {
    "id": 1,
    "name": "str",
    "start_time": "str",
    "finished": True,
    "category_id": 1,
    "account_id": 1,
    "pom_finished": 1,
}


class MockTaskQueries:
    def get_tasks(self):
        return task_get_list

    def create_task(self, item):
        if item.account_id != None:
            return task
        else:
            raise Exception


req_body_good = {
    "id": 1,
    "name": "str",
    "start_time": "str",
    "finished": True,
    "category_id": 1,
    "account_id": 1,
    "pom_finished": 1,
}
req_body_bad = {
    "id": 1,
    "name": "str",
    "start_time": "str",
    "finished": True,
    "category_id": 1,
}


def test_get_task():
    app.dependency_overrides[TaskRepository] = MockTaskQueries
    response = client.get("/api/tasks")

    assert response.status_code == 200
    assert response.json() == task_get_list

    app.dependency_overrides = {}


def test_create_task():
    app.dependency_overrides[TaskRepository] = MockTaskQueries

    response = client.post("/api/tasks", json=req_body_good)

    assert response.status_code == 200
    assert response.json() == task

    try:
        response = client.post("/api/tasks", json=req_body_bad)
    except:
        assert response.status_code == 422
        assert response.json() == None

    app.dependecy_overrides = {}
