from fastapi import FastAPI
from authenticator import authenticator
from routers import accounts, tasks
from fastapi.middleware.cors import CORSMiddleware
import os

app = FastAPI()
app.include_router(authenticator.router)
app.include_router(accounts.router)
app.include_router(tasks.router)


app.add_middleware(
    CORSMiddleware,
    allow_origins=["http://localhost:3000", os.environ.get("CORS_HOST", None)],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

# @app.get("/api/launch-details")
# def launch_details():
#     return {
#         "launch_details": {
#             "year": 2022,
#             "month": 10,
#             "day": "28",
#             "hour": 19,
#             "min": 0,
#             "tz:": "PST"
#         }
#     }
